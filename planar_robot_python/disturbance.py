import rclpy
from rclpy.node import Node
from sensor_msgs.msg import JointState
from custom_messages.msg import KinData
from custom_messages.msg import CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np

class Disturbance(Node):
    def __init__(self):
        super().__init__('disturbance')
        self.declare_parameter('A',0.0)
        self.declare_parameter('w', 10.0)
        self.publisher_     = self.create_publisher(JointState, 'qdot_disturbance', 10)
       

        
        self.sampling_period    = 0.01
        self.timer              = self.create_timer(self.sampling_period, self.timer_callback)
        self.initialized        = False
        self.qdt                =np.zeros(2)
        self.dt                 =0.0

    

    def timer_callback(self):
        now                         = self.get_clock().now()
        self.dt                     +=self.sampling_period
     
        A                           = self.get_parameter('A').value
        w                           = self.get_parameter('w').value
        self.qdt[0]                 = A* np.sin(w*self.dt)
        self.qdt[1]                 = A*np.sin((w/2)*self.dt)


        joint_state                 = JointState()
        joint_state.velocity        = self.qdt
        self.publisher_.publish(joint_state)

def main(args=None):
    rclpy.init(args=args)
    disturbance = Disturbance()
    rclpy.spin(disturbance)
    disturbance.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()