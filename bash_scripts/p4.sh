. install/setup.zsh
ros2 run planar_robot_python kinematic_model &
sleep 1
ros2 topic echo \kin_data &

sleep 1
echo "\n=============================================================="
echo "[Enter] ros2 topic pub q = 1, 1"
read
ros2 topic pub /joint_states sensor_msgs/msg/JointState "{name: ['r1','r2'], position: [1.0,1.0]}" --once
sleep 2
echo "Expected result"
echo "x: 0.12415546932099736"
echo "y: 1.7507684116335782"
echo "jacobian:"
echo "- -1.7507684116335782"
echo "- -0.9092974268256817"
echo "- 0.12415546932099736"
echo "- -0.4161468365471424"

sleep 2
echo "\n=============================================================="
echo "[Enter] ros2 topic pub q = 1.5, 2.0"
read
ros2 topic pub /joint_states sensor_msgs/msg/JointState "{name: ['r1','r2'], position: [1.5,2.0]}" --once
sleep 2
echo "Expected result"
echo "x: -0.8657194856230934"
echo "y: 0.6467117589144347"
echo "jacobian:"
echo "- -0.6467117589144347"
echo "- 0.35078322768961984"
echo "- -0.8657194856230934"
echo "- -0.9364566872907963"

sleep 2
echo "\n==============================================================\n"
echo "[Enter] kill all ROS2 processes"
read 

killall kinematic_model
killall ros2 